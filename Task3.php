<?php
class MyCalculator{
    public $num1;
    public $num2;
    public function __construct($var1,$var2)
    {
        $this->num1=$var1;
        $this->num2=$var2;
    }

    public function add(){

        return $this->num1+$this->num2;

    }
    public function subtract(){

        return $this->num1-$this->num2;

    }
    public function multiply(){

        return $this->num1*$this->num2;

    }
    public function divide(){

        return $this->num1/$this->num2;

    }

}
$obj = new MyCalculator(12, 6);
echo "Addition is :". $obj->add()."<br>";
echo "Substract is :" .$obj->subtract()."<br>";
echo "Multiply is :".$obj->multiply()."<br>";
echo "Division is :".$obj->divide();